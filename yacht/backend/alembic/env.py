from alembic import context
from logging.config import fileConfig
from os.path import abspath, dirname
from sqlalchemy import engine_from_config, MetaData
from sqlalchemy import pool
import os
import sys

config = context.config
fileConfig(config.config_file_name)

sys.path.insert(0, dirname(dirname(abspath(__file__))))
from api.db import models

combined_meta_data = MetaData()
for declarative_base in [models.Base]:
    for table_name, table in declarative_base.metadata.tables.items():
        combined_meta_data._add_table(table_name, table.schema, table)
target_metadata = combined_meta_data
config.set_main_option(
    "sqlalchemy.url", os.environ.get("DATABASE_URL", "sqlite:///config/data.sqlite")
)


def run_migrations_offline():
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )
    with context.begin_transaction():
        context.execute("DROP TABLE IF EXISTS alembic_version;")
        context.run_migrations()


def run_migrations_online():
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )
    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata)
        with context.begin_transaction():
            context.execute("DROP TABLE IF EXISTS alembic_version;")
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
