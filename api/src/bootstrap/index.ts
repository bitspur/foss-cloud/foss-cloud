import 'nestjs-axios-logger/axiosInherit';
import dotenv from 'dotenv';
import path from 'path';
import type { RegisterAppModuleConfig } from 'app/app.module';
import { AppModule } from 'app/app.module';
import { ConfigService } from '@nestjs/config';
import type { NestExpressApplication } from '@nestjs/platform-express';
import { ExpressAdapter } from '@nestjs/platform-express';
import { NestFactory } from '@nestjs/core';
import type { NestApplicationOptions } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { logLevels, registerLogger } from './logger';
import { registerSwagger } from './swagger';

dotenv.config({ path: path.resolve(process.cwd(), '../.env') });

const logger = new Logger('Bootstrap');
let app: NestExpressApplication;

export async function createApp(
  config: CreateAppConfig = {},
): Promise<NestExpressApplication> {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule.register(),
    new ExpressAdapter(),
    {
      bodyParser: true,
      logger: logLevels,
      ...(config.nest || {}),
    },
  );
  const configService = app.get(ConfigService);
  // app.enableShutdownHooks();
  // app.useGlobalPipes(new ValidationPipe());
  if (configService.get('CORS') === '1') {
    app.enableCors({ origin: configService.get('CORS_ORIGIN') || '*' });
  }
  return app;
}

export async function appListen(app: NestExpressApplication) {
  const configService = app.get(ConfigService);
  const port = Number(configService.get('PORT') || 5000);
  await app
    .listen(port, '0.0.0.0', () => {
      logger.log(`listening on port ${port}`);
    })
    .catch(logger.error);
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}

export async function start() {
  app = await createApp({
    nest: {
      bufferLogs: false,
    },
    appModule: {
      registerKeycloak: true,
    },
  });
  await registerLogger(app);
  await registerSwagger(app);
  await appListen(app);
}

export async function stop() {
  if (!app) return;
  await app.close();
}

export async function restart() {
  await stop();
  await start();
}

declare const module: any;

export interface CreateAppConfig {
  nest?: NestApplicationOptions;
  appModule?: RegisterAppModuleConfig;
}
