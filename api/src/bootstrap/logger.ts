import type { LogLevel } from '@nestjs/common';
import type { NestExpressApplication } from '@nestjs/platform-express';
import { Logger as PinoLogger } from 'nestjs-pino';

const { env } = process;

export async function registerLogger(app: NestExpressApplication) {
  app.useLogger(app.get(PinoLogger));
}

export let logLevels = (env.LOG_LEVELS || '')
  .split(',')
  .filter((l) => l) as LogLevel[];
if (!logLevels.length || !!Number(env.DEBUG)) {
  logLevels = ['error', 'warn', 'log', 'debug', 'verbose'];
}
