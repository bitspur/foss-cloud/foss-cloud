import type { DynamicModule } from '@nestjs/common';
import path from 'path';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Global, Module } from '@nestjs/common';
import modules from './modules';
import coreModules from './modules/core';
import { ZodValidationPipe } from 'nestjs-zod';
import { APP_PIPE } from '@nestjs/core';

@Global()
@Module({})
export class AppModule {
  public static register(_config: RegisterAppModuleConfig = {}): DynamicModule {
    return {
      global: true,
      controllers: [],
      imports: [
        ConfigModule.forRoot({
          envFilePath: path.resolve(process.cwd(), '../.env'),
        }),
        ...coreModules,
        ...modules,
      ],
      module: AppModule,
      providers: [
        ConfigService,
        {
          provide: APP_PIPE,
          useClass: ZodValidationPipe,
        },
      ],
      exports: [ConfigService],
    };
  }
}

export interface RegisterAppModuleConfig {}
