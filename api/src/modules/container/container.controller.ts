import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { DeployBodyDto } from './container.dto';
import { ContainerService } from './container.service';
import { ApiTags } from '@nestjs/swagger';

@Controller('containers')
@ApiTags('containers')
export class ContainerController {
  constructor(private containerService: ContainerService) {}
  @Get()
  async getContainers() {
    return this.containerService.getContainers();
  }

  @Get(':container_name')
  async getContainer(@Param('container_name') container_name: string) {
    return this.containerService.getContainer(container_name);
  }

  @Get(':container_name/processes')
  async getProcesses(@Param('container_name') container_name: string) {
    return this.containerService.getContainerProcesses(container_name);
  }

  @Get(':container_name/check-updates')
  async checkUpdates(@Param('container_name') container_name: string) {
    return this.containerService.checkContainerUpdates(container_name);
  }

  @Get(':container_name/update')
  async updateContainer(@Param('container_name') container_name: string) {
    return this.containerService.updateContainer(container_name);
  }

  @Get('actions/:container_name/:action')
  async getActionOnContainer(
    @Param('container_name') container_name: string,
    @Param('action') action: string,
  ) {
    return this.containerService.getActionOnContainer(container_name, action);
  }

  @Post('deploy')
  async deployContainer(@Body() containerBody: DeployBodyDto) {
    return this.containerService.deployContainer(containerBody);
  }

  @Get(':container_name/logs')
  async getLogs(
    @Param('container_name') container_name: string,
    @Res() res: Response,
  ) {
    return (await this.containerService.getLogs(container_name)).pipe(res);
  }

  @Get(':container_name/stats')
  async getStatsByContainerName(
    @Param('container_name') container_name: string,
    @Res() res: Response,
  ) {
    return (
      await this.containerService.getStatsByContainerName(container_name)
    ).pipe(res);
  }

  @Get('stats')
  async getStats(@Res() res: Response) {
    return (await this.containerService.getStats()).pipe(res);
  }

  @Get(':container_name/support')
  async getSupport(@Param('container_name') container_name: string) {
    return this.containerService.getSupport(container_name);
  }
}
