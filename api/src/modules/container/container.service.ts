import { Injectable } from '@nestjs/common';
import { AxiosService } from '../core/axios';
import type { DeployBodyDto } from './container.dto';

@Injectable()
export class ContainerService {
  constructor(private axiosService: AxiosService) {}

  async getContainers() {
    return (await this.axiosService.api.get('/apps')).data;
  }

  async getContainer(container: string) {
    return (await this.axiosService.api.get(`/apps/${container}`)).data;
  }

  async getContainerProcesses(container_name: string) {
    return (
      await this.axiosService.api.get(`/apps/${container_name}/processes`)
    ).data;
  }

  async getActionOnContainer(container_name: string, action: string) {
    return (
      await this.axiosService.api.get(
        `/apps/actions/${container_name}/${action}`,
      )
    ).data;
  }

  async deployContainer(containerBody: DeployBodyDto) {
    return (await this.axiosService.api.post(`/apps/deploy`, containerBody))
      .data;
  }

  async checkContainerUpdates(container_name: string) {
    return (await this.axiosService.api.get(`/apps/${container_name}/updates`))
      .data;
  }

  async updateContainer(container_name: string) {
    return (await this.axiosService.api.post(`/apps/${container_name}/update`))
      .data;
  }

  async getLogs(container_name: string) {
    return (
      await this.axiosService.api.get(`/apps/${container_name}/logs`, {
        responseType: 'stream',
      })
    ).data;
  }

  async getStats() {
    return (
      await this.axiosService.api.get(`/apps/stats`, { responseType: 'stream' })
    ).data;
  }

  async getStatsByContainerName(container_name: string) {
    return (
      await this.axiosService.api.get(`/apps/${container_name}/stats`, {
        responseType: 'stream',
      })
    ).data;
  }

  async getSupport(container_name: string) {
    return (await this.axiosService.api.get(`/apps/${container_name}/support`))
      .data;
  }
}
