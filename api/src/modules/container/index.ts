import { Module } from '@nestjs/common';
import { ContainerController } from './container.controller';
import { ContainerService } from './container.service';
import { AxiosService } from '../core/axios';

@Module({
  controllers: [ContainerController],
  providers: [ContainerService, AxiosService],
})
export class ContainerModule {}
