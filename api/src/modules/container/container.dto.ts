import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const PortsSchema = z.object({
  label: z.optional(z.string()),
  cport: z.string(),
  hport: z.optional(z.string()),
  proto: z.string(),
});

const VolumesSchema = z.object({
  container: z.string(),
  bind: z.string(),
});

const EnvSchema = z.object({
  label: z.string(),
  default: z.optional(z.string()),
  name: z.optional(z.string()),
  description: z.optional(z.string()),
});

const SysctlsSchema = z.object({
  name: z.string(),
  value: z.string(),
});

const DevicesSchema = z.object({
  container: z.string(),
  host: z.string(),
});

const LabelSchema = z.object({
  label: z.string(),
  value: z.string(),
});

const DeployBodySchema = z.object({
  name: z.string(),
  image: z.string(),
  restart_policy: z.string(),
  notes: z.optional(z.string()),
  command: z.optional(z.array(z.string())),
  ports: z.optional(z.array(PortsSchema)),
  volumes: z.optional(z.array(VolumesSchema)),
  env: z.optional(z.array(EnvSchema)),
  devices: z.optional(z.array(DevicesSchema)),
  labels: z.optional(z.array(LabelSchema)),
  sysctls: z.optional(z.array(SysctlsSchema)),
  cap_add: z.optional(z.array(z.string())),
  network_mode: z.optional(z.string()),
  network: z.optional(z.string()),
  cpus: z.optional(z.number()),
  mem_limit: z.optional(z.string()),
  edit: z.optional(z.boolean()),
  id: z.optional(z.string()),
});

export class DeployBodyDto extends createZodDto(DeployBodySchema) {}
