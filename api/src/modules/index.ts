import { ComposeModule } from './compose';
import { ContainerModule } from './container';
import { ResourcesModule } from './resources';

export default [ContainerModule, ComposeModule, ResourcesModule];
