import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const ImageSchema = z.object({
  image: z.string(),
});

export class ImageDto extends createZodDto(ImageSchema) {}
