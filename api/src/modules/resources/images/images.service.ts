import { Injectable } from '@nestjs/common';
import { AxiosService } from 'app/modules/core/axios';
import type { ImageDto } from './images.dto';

@Injectable()
export class ImagesService {
  constructor(private axiosService: AxiosService) {}

  async getImages() {
    return (await this.axiosService.api.get('/resources/images')).data;
  }

  async getImageById(image_id: string) {
    return (await this.axiosService.api.get(`/resources/images/${image_id}`))
      .data;
  }

  async writeImage(body: ImageDto) {
    return (await this.axiosService.api.post('/resources/images', body)).data;
  }

  async pullImage(image_id: string) {
    return (
      await this.axiosService.api.get(`/resources/images/${image_id}/pull`)
    ).data;
  }

  async deleteImage(image_id: string) {
    return (await this.axiosService.api.delete(`/resources/images/${image_id}`))
      .data;
  }
}
