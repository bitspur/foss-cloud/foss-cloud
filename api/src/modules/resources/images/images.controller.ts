import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ImagesService } from './images.service';
import { ImageDto } from './images.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('resources/images')
@ApiTags('images')
export class ImagesController {
  constructor(private imagesService: ImagesService) {}

  @Get()
  async getImages() {
    return this.imagesService.getImages();
  }

  @Post('edit')
  async writeImage(@Body() body: ImageDto) {
    return this.imagesService.writeImage(body);
  }

  @Get(':image_id')
  async getImageById(@Param('image_id') image_id: string) {
    return this.imagesService.getImageById(image_id);
  }

  @Get(':image_id/pull')
  async pullImage(@Param('image_id') image_id: string) {
    return this.imagesService.pullImage(image_id);
  }

  @Delete(':image_id/delete')
  async deleteImage(@Param('image_id') image_id: string) {
    return this.imagesService.deleteImage(image_id);
  }
}
