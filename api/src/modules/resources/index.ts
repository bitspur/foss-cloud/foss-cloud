import { Module } from '@nestjs/common';
import { ImagesController } from './images/images.controller';
import { NetworksController } from './networks/networks.controller';
import { VolumesController } from './volumes/volumes.controller';
import { ImagesService } from './images/images.service';
import { NetworksService } from './networks/networks.service';
import { VolumesService } from './volumes/volumes.service';
import { AxiosService } from '../core/axios';

@Module({
  controllers: [ImagesController, NetworksController, VolumesController],
  providers: [ImagesService, NetworksService, VolumesService, AxiosService],
})
export class ResourcesModule {}
