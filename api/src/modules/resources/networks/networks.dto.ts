import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const NetworkSchema = z.object({
  attachable: z.boolean(),
  internal: z.boolean(),
  ipv4gateway: z.string(),
  ipv4range: z.optional(z.string()),
  ipv4subnet: z.string(),
  ipv6_enabled: z.boolean(),
  ipv6gateway: z.optional(z.string()),
  ipv6range: z.optional(z.string()),
  ipv6subnet: z.optional(z.string()),
  name: z.string(),
  networkDriver: z.string(),
  network_devices: z.string(),
});

export class NetworkDto extends createZodDto(NetworkSchema) {}
