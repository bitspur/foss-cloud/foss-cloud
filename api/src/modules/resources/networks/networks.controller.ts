import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { NetworkDto } from './networks.dto';
import { NetworksService } from './networks.service';
import { ApiTags } from '@nestjs/swagger';

@Controller('networks')
@ApiTags('networks')
export class NetworksController {
  constructor(private networksService: NetworksService) {}
  @Get()
  async getNetworks() {
    return this.networksService.getNetworks();
  }

  @Post('edit')
  async writeNetwork(@Body() body: NetworkDto) {
    return this.networksService.writeNetwork(body);
  }

  @Get(':network_name')
  async getNetworkByName(@Param('network_name') network_name: string) {
    return this.networksService.getNetworkByName(network_name);
  }

  @Delete(':network_name/delete')
  async deleteNetwork(@Param('network_name') network_name: string) {
    return this.networksService.deleteNetwork(network_name);
  }
}
