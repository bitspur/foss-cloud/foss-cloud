import { Injectable } from '@nestjs/common';
import { AxiosService } from 'app/modules/core/axios';
import type { NetworkDto } from './networks.dto';

@Injectable()
export class NetworksService {
  constructor(private axiosService: AxiosService) {}

  async getNetworks() {
    return (await this.axiosService.api.get('/resources/networks')).data;
  }

  async writeNetwork(body: NetworkDto) {
    return (await this.axiosService.api.post('/resources/networks', body)).data;
  }

  async getNetworkByName(network_name: string) {
    return (
      await this.axiosService.api.get(`/resources/networks/${network_name}`)
    ).data;
  }

  async deleteNetwork(network_name: string) {
    return (
      await this.axiosService.api.delete(`/resources/networks/${network_name}`)
    ).data;
  }
}
