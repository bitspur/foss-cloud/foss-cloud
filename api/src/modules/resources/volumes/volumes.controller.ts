import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { VolumesService } from './volumes.service';
import { VolumeDto } from './volumes.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('resources/volumes')
@ApiTags('volumes')
export class VolumesController {
  constructor(private volumesService: VolumesService) {}

  @Get()
  async getVolumes() {
    return this.volumesService.getVolumes();
  }

  @Post('edit')
  async writeVolume(@Body() body: VolumeDto) {
    return this.volumesService.writeVolume(body);
  }

  @Get(':volume_name')
  async getVolumeByName(@Param('volume_name') volume_name: string) {
    return this.volumesService.getVolumeByName(volume_name);
  }

  @Delete(':volume_name/delete')
  async deleteVolume(@Param('volume_name') volume_name: string) {
    return this.volumesService.deleteVolume(volume_name);
  }
}
