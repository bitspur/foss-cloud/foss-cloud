import { Injectable } from '@nestjs/common';
import { AxiosService } from 'app/modules/core/axios';
import type { VolumeDto } from './volumes.dto';

@Injectable()
export class VolumesService {
  constructor(private axiosService: AxiosService) {}

  async getVolumes() {
    return (await this.axiosService.api.get('/resources/volumes')).data;
  }

  async writeVolume(body: VolumeDto) {
    return (await this.axiosService.api.post('/resources/volumes', body)).data;
  }

  async getVolumeByName(volume_name: string) {
    return (
      await this.axiosService.api.get(`/resources/volumes/${volume_name}`)
    ).data;
  }

  async deleteVolume(volume_name: string) {
    return (
      await this.axiosService.api.delete(`/resources/volumes/${volume_name}`)
    ).data;
  }
}
