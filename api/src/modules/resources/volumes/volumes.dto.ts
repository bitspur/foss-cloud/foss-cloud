import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const VolumeSchema = z.object({
  name: z.string(),
});

export class VolumeDto extends createZodDto(VolumeSchema) {}
