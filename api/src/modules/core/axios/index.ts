import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosInstance } from 'axios';

@Injectable()
export class AxiosService {
  api: AxiosInstance;
  constructor(config: ConfigService) {
    this.api = axios.create({
      baseURL: config.get('YACHT_API_BASE_URL') || '',
    });
  }
}
