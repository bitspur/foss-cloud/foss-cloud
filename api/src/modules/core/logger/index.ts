import { AxiosLoggerModule } from 'nestjs-axios-logger';
import { ConfigService } from '@nestjs/config';
import type { DynamicModule } from '@nestjs/common/interfaces';
import type { IncomingMessage, ServerResponse } from 'http';
import { LoggerModule as PinoLoggerModule } from 'nestjs-pino';
import { Global, Module, RequestMethod } from '@nestjs/common';
import { createPinoHttp } from './logger';

const imports = [
  AxiosLoggerModule.registerAsync({
    inject: [ConfigService],
    useFactory(config: ConfigService) {
      return {
        data: config.get('LOG_AXIOS_DATA') === '1',
        headers: config.get('LOG_AXIOS_HEADERS') === '1',
        requestLogLevel: 'log',
        responseLogLevel: 'log',
      };
    },
  }),
];

@Global()
@Module({
  imports: [createPinoLoggerModule(), ...imports],
})
export class LoggerModule {
  public static register(options: LoggerModuleOptions = {}): DynamicModule {
    return {
      imports: [createPinoLoggerModule(options), ...imports],
      global: true,
      module: LoggerModule,
    };
  }
}

export interface LoggerModuleOptions {
  color?: boolean;
  httpMixin?: (
    mergeObject: object,
    req: IncomingMessage,
    res: ServerResponse<IncomingMessage>,
  ) => object;
  ignore?: string[];
  mixin?: (mergeObject: object, level: number) => object;
  prettifiers?: Record<string, (data: string | object) => string>;
  strings?: string[];
}

function createPinoLoggerModule(options: LoggerModuleOptions = {}) {
  return PinoLoggerModule.forRootAsync({
    inject: [ConfigService],
    useFactory(config: ConfigService) {
      return {
        pinoHttp: createPinoHttp(config, options),
        exclude: [{ method: RequestMethod.ALL, path: 'health' }],
      };
    },
  });
}
