import { Injectable } from '@nestjs/common';
import { AxiosService } from '../core/axios';
import type { ComposeDto } from './compose.dto';

@Injectable()
export class ComposeService {
  constructor(private axiosService: AxiosService) {}
  async getComposes() {
    return (await this.axiosService.api.get('/compose')).data;
  }

  async getComposeByName(compose_name: string) {
    return (await this.axiosService.api.get(`/compose/${compose_name}`)).data;
  }

  async getActionOnCompose(compose_name: string, action: string) {
    return (
      await this.axiosService.api.get(
        `/compose/${compose_name}/actions/${action}`,
      )
    ).data;
  }

  async getActionOnComposeService(
    compose_name: string,
    action: string,
    service_name: string,
  ) {
    return (
      await this.axiosService.api.get(
        `/compose/${compose_name}/actions/${action}/${service_name}`,
      )
    ).data;
  }

  async editOrWriteCompose(compose_name: string, body: ComposeDto) {
    return (
      await this.axiosService.api.post(`/compose/${compose_name}/edit`, body)
    ).data;
  }

  async getSupport(compose_name: string) {
    return (await this.axiosService.api.get(`/compose/${compose_name}/support`))
      .data;
  }
}
