import { Test } from '@nestjs/testing';
import type { TestingModule } from '@nestjs/testing';
import { ComposeService } from './compose.service';

describe('ComposeService', () => {
  let service: ComposeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ComposeService],
    }).compile();

    service = module.get<ComposeService>(ComposeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
