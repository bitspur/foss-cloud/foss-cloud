import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ComposeService } from './compose.service';
import { ComposeDto } from './compose.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('compose')
@ApiTags('compose')
export class ComposeController {
  constructor(private composeService: ComposeService) {}
  @Get()
  async getComposes() {
    return this.composeService.getComposes();
  }

  @Get(':compose_name')
  async getComposeByName(@Param('compose_name') compose_name: string) {
    return this.composeService.getComposeByName(compose_name);
  }

  @Post(':compose_name/upsert')
  async upsertCompose(
    @Param('compose_name') compose_name: string,
    @Body() body: ComposeDto,
  ) {
    return this.composeService.editOrWriteCompose(compose_name, body);
  }

  @Get(':compose_name/actions/:action')
  async getActionOnCompose(
    @Param('compose_name') compose_name: string,
    @Param('action') action: string,
  ) {
    return this.composeService.getActionOnCompose(compose_name, action);
  }

  @Get(':compose_name/actions/:action/:service_name')
  async getActionOnComposeService(
    @Param('compose_name') compose_name: string,
    @Param('action') action: string,
    @Param('service_name') service_name: string,
  ) {
    return this.composeService.getActionOnComposeService(
      compose_name,
      action,
      service_name,
    );
  }

  @Get(':compose_name/support')
  async getSupport(@Param('compose_name') compose_name: string) {
    return this.composeService.getSupport(compose_name);
  }
}
