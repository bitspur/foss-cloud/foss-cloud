import { Test } from '@nestjs/testing';
import type { TestingModule } from '@nestjs/testing';
import { ComposeController } from './compose.controller';

describe('ComposeController', () => {
  let controller: ComposeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ComposeController],
    }).compile();

    controller = module.get<ComposeController>(ComposeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
