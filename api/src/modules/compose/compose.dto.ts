import { z } from 'nestjs-zod/z';
import { createZodDto } from 'nestjs-zod';

const composeSchema = z.object({
  name: z.string(),
  content: z.string(),
});

export class ComposeDto extends createZodDto(composeSchema) {}
