import { Module } from '@nestjs/common';
import { ComposeController } from './compose.controller';
import { ComposeService } from './compose.service';
import { AxiosService } from '../core/axios';

@Module({
  controllers: [ComposeController],
  providers: [ComposeService, AxiosService],
})
export class ComposeModule {}
