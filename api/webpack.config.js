const nodeExternals = require('webpack-node-externals');
const path = require('path');
const transpileModules = require('./transpileModules');

module.exports = {
  mode: 'development',
  target: 'node',
  devtool: 'eval-source-map',
  entry: {
    main: './src/main.ts',
  },
  externalsPresets: {
    node: true,
  },
  externals: [
    nodeExternals({
      allowlist: transpileModules,
    }),
  ],
  resolve: {
    extensions: ['.cjs', '.js', '.json', '.jsx', '.mjs', '.ts', '.tsx'],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].js',
  },
  plugins: [],
  module: {
    rules: [
      {
        test: /\.(([mc]?js)|([jt]sx?))$/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: true,
          },
        },
      },
    ],
  },
};
